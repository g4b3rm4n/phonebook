# Sending API request through `curl`

```
curl  --header "Content-Type: application/json" --request POST --data '{ "phone_number": "+36305336398", "person_id": "1" }' http://127.0.0.1:8000/api/phoneNumbers
```