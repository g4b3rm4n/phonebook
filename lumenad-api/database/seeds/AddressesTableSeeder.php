<?php

use Illuminate\Database\Seeder;
use App\Address;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Address::truncate();

        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 20; $i++) {
            Address::create([
                'country' => $faker->optional($weight = 0.3)->country,
                'city' => $faker->optional($weight = 0.3)->city,
                'region' => $faker->optional($weight = 0.3)->state,
                'zip_code' => $faker->optional($weight = 0.3)->postcode,
                'street' => $faker->optional($weight = 0.3)->streetName,
                'house_number' => $faker->optional($weight = 0.3)->buildingNumber,
                'person_id' => $faker->numberBetween($min = 1, $max = 10),
            ]);
        }
    }
}
