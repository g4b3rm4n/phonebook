<?php

use Illuminate\Database\Seeder;
use App\Email;

class EmailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Email::truncate();

        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 20; $i++) {
            Email::create([
                'email_address' => $faker->email,
                'person_id' => $faker->numberBetween($min = 1, $max = 10),
            ]);
        }
    }
}
