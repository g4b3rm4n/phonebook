<?php

use Illuminate\Database\Seeder;
use App\PhoneNumber;

class PhoneNumbersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PhoneNumber::truncate();

        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 20; $i++) {
            PhoneNumber::create([
                'phone_number' => $faker->e164PhoneNumber,
                'person_id' => $faker->numberBetween($min = 1, $max = 10),
            ]);
        }
    }
}
