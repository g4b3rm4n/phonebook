<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('people', 'PersonController@index');
Route::get('people/{id}', 'PersonController@show');
Route::post('people', 'PersonController@store');
Route::put('people/{id}', 'PersonController@update');
Route::delete('people/{id}', 'PersonController@delete');

Route::get('person/{id}', 'PersonController@withAll');

Route::get('emails', 'EmailController@index');
Route::get('emails/{id}', 'EmailController@show');
Route::post('emails', 'EmailController@store');
Route::put('emails/{id}', 'EmailController@update');
Route::delete('emails/{id}', 'EmailController@delete');

Route::get('phoneNumbers', 'PhoneNumberController@index');
Route::get('phoneNumbers/{id}', 'PhoneNumberController@show');
Route::post('phoneNumbers', 'PhoneNumberController@store');
Route::put('phoneNumbers/{id}', 'PhoneNumberController@update');
Route::delete('phoneNumbers/{id}', 'PhoneNumberController@delete');

Route::get('addresses', 'AddressController@index');
Route::get('addresses/{id}', 'AddressController@show');
Route::post('addresses', 'AddressController@store');
Route::put('addresses/{id}', 'AddressController@update');
Route::delete('addresses/{id}', 'AddressController@delete');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
