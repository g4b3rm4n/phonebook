<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $primaryKey = 'email_id';
    protected $fillable = ['email_address', 'person_id'];

    public $timestamps = false;

    public function person()
    {
        return $this->belongsTo('App\Person', 'foreign_key', 'person_id');
    }
}
