<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{
    protected $primaryKey = 'phone_number_id';
    protected $fillable = ['phone_number', 'person_id'];
    
    public $timestamps = false;
    
    public function person()
    {
        return $this->belongsTo('App\Person', 'foreign_key', 'person_id');
    }
}
