<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $primaryKey = 'person_id';
    protected $fillable = ['first_name', 'last_name'];

    public $timestamps = false;
    
    public function emails()
    {
        return $this->hasMany(Email::class, 'person_id', 'person_id');
    }

    public function addresses()
    {
        return $this->hasMany(Address::class, 'person_id', 'person_id');
    }

    public function phone_numbers()
    {
        return $this->hasMany(PhoneNumber::class, 'person_id', 'person_id');
    }
}
