<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PhoneNumber;

class PhoneNumberController extends Controller
{
    public function index()
    {
        return PhoneNumber::all();
    }
 
    public function show($id)
    {
        return PhoneNumber::find($id);
    }

    public function store(Request $request)
    {
        return PhoneNumber::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $phoneNumber = PhoneNumber::findOrFail($id);
        $phoneNumber->update($request->all());

        return $phoneNumber;
    }

    public function delete(Request $request, $id)
    {
        $phoneNumber = PhoneNumber::findOrFail($id);
        $phoneNumber->delete();

        return 204;
    }
}
