<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;

class EmailController extends Controller
{
    public function index()
    {
        return Email::all();
    }
 
    public function show($id)
    {
        return Email::find($id);
    }

    public function store(Request $request)
    {
        return Email::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $email = Email::findOrFail($id);
        $email->update($request->all());

        return $email;
    }

    public function delete(Request $request, $id)
    {
        $email = Email::findOrFail($id);
        $email->delete();

        return 204;
    }
}
