<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $primaryKey = 'address_id';
    protected $fillable = ['country', 'city', 'region', 'zip_code', 'street', 'house_number', 'person_id'];

    public $timestamps = false;

    public function person()
    {
        return $this->belongsTo('App\Person', 'foreign_key', 'person_id');
    }
}
