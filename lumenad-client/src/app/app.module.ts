import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PhonebookComponent } from './components/phonebook/phonebook.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PersonService } from './services/person.service';
import { EmailService } from './services/email.service';
import { AddressService } from './services/address.service';
import { PhoneNumberService } from './services/phone-number.service';
import { PersonComponent } from './components/person/person.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    PhonebookComponent,
    PageNotFoundComponent,
    PersonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    PersonService,
    EmailService,
    AddressService,
    PhoneNumberService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
