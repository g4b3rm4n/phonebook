import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';

import { Person } from '../classes/person';
import { IApiPerson } from '../interfaces/api-person.interface';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private peopleUrl: string;
  private personUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.peopleUrl = '/api/people';
    this.personUrl = '/api/person';
  }

  public getPeople(): Observable<Person[]> {
    const processResponse = (people: IApiPerson[]): Person[] => {
      return people.map((p) => new Person(p));
    }
    return this.http.get(this.peopleUrl).pipe(map(processResponse));
  }

  public addPerson(person: Person): Observable<Person> {
    const processResponse = (person: IApiPerson): Person => {
      return new Person(person);
    }
    return this.http.post(
      this.peopleUrl,
      Person.convertToApiObject(person)
    ).pipe(map(processResponse));
  }

  public getPerson(id: number): Observable<Person> {
    const processResponse = (person: IApiPerson): Person => {
      return new Person(person);
    }
    return this.http.get(this.personUrl + '/' + id).pipe(map(processResponse));
  }

  public modifyPerson(person: Person): Observable<Person> {
    const processResponse = (person: IApiPerson): Person => {
      return new Person(person);
    }
    return this.http.put(
      this.peopleUrl + '/' + person.personId,
      Person.convertToApiObject(person)
    ).pipe(map(processResponse));
  }

  public deletePerson(person: Person): Observable<void> {
    return this.http.delete(this.peopleUrl + '/' + person.personId).pipe(mapTo(undefined));
  }
}
