import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Address } from '../classes/address';
import { Observable } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';
import { IApiAddress } from '../interfaces/api-address.interface';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private addresssUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.addresssUrl = '/api/addresses';
  }

  public addAddress(address: Address): Observable<Address> {
    const processResponse = (person: IApiAddress): Address => {
      return new Address(person);
    }
    return this.http.post(
      this.addresssUrl,
      Address.convertToApiObject(address)
    ).pipe(map(processResponse));
  }

  public modifyAddress(address: Address): Observable<Address> {
    const processResponse = (address: IApiAddress): Address => {
      return new Address(address);
    }
    return this.http.put(
      this.addresssUrl + '/' + address.addressId,
      Address.convertToApiObject(address)
    ).pipe(map(processResponse));
  }

  public deleteAddress(address: Address): Observable<void> {
    return this.http.delete(this.addresssUrl + '/' + address.addressId).pipe(mapTo(undefined));
  }
}
