import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Email } from '../classes/email';
import { IApiEmail } from '../interfaces/api-email.interface';
import { Observable } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  private emailsUrl: string;

  constructor(
    private http: HttpClient
  ) { 
    this.emailsUrl = '/api/emails';
  }

  public addEmail(email: Email): Observable<Email> {
    const processResponse = (person: IApiEmail): Email => {
      return new Email(person);
    }
    return this.http.post(
      this.emailsUrl,
      Email.convertToApiObject(email)
    ).pipe(map(processResponse));
  }

  public modifyEmail(email: Email): Observable<Email> {
    const processResponse = (email: IApiEmail): Email => {
      return new Email(email);
    }
    return this.http.put(
      this.emailsUrl + '/' + email.emailId,
      Email.convertToApiObject(email)
    ).pipe(map(processResponse));
  }

  public deleteEmail(email: Email): Observable<void> {
    return this.http.delete(this.emailsUrl + '/' + email.emailId).pipe(mapTo(undefined));
  }
}
