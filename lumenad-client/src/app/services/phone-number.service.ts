import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PhoneNumber } from '../classes/phone-number';
import { Observable } from 'rxjs';
import { IApiPhoneNumber } from '../interfaces/api-phone-number.interface';
import { map, mapTo } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PhoneNumberService {

  private phoneNumbersUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.phoneNumbersUrl = '/api/phoneNumbers';
  }

  public addPhoneNumber(phoneNumber: PhoneNumber): Observable<PhoneNumber> {
    const processResponse = (person: IApiPhoneNumber): PhoneNumber => {
      return new PhoneNumber(person);
    }
    return this.http.post(
      this.phoneNumbersUrl,
      PhoneNumber.convertToApiObject(phoneNumber)
    ).pipe(map(processResponse));
  }

  public modifyPhoneNumber(phoneNumber: PhoneNumber): Observable<PhoneNumber> {
    const processResponse = (phoneNumber: IApiPhoneNumber): PhoneNumber => {
      return new PhoneNumber(phoneNumber);
    }
    return this.http.put(
      this.phoneNumbersUrl + '/' + phoneNumber.phoneNumberId,
      PhoneNumber.convertToApiObject(phoneNumber)
    ).pipe(map(processResponse));
  }

  public deletePhoneNumber(phoneNumber: PhoneNumber): Observable<void> {
    return this.http.delete(this.phoneNumbersUrl + '/' + phoneNumber.phoneNumberId).pipe(mapTo(undefined));
  }
}
