import { IApiAddress } from '../interfaces/api-address.interface';

export class Address {
  public addressId: number;
  public country: string;
  public city: string;
  public region: string;
  public zipCode: string;
  public street: string;
  public houseNumber: number;
  public personId: number;

  constructor(address: IApiAddress) {
    this.addressId = address.address_id;
    this.country = address.country;
    this.city = address.city;
    this.region = address.region;
    this.zipCode = address.zip_code;
    this.street = address.street;
    this.houseNumber = address.house_number;
    this.personId = address.person_id;
  }

  public static convertToApiObject(address: Address): IApiAddress {
    return {
      address_id: address.addressId,
      country: address.country,
      city: address.city,
      region: address.region,
      zip_code: address.zipCode,
      street: address.street,
      house_number: address.houseNumber,
      person_id: address.personId
    };
  }
}