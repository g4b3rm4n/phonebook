import { IApiEmail } from '../interfaces/api-email.interface';

export class Email {
  public emailId: number;
  public emailAddress: string;
  public personId: number;

  constructor(email: IApiEmail) {
    this.emailId = email.email_id;
    this.emailAddress = email.email_address;
    this.personId = email.person_id;
  }

  public static convertToApiObject(email: Email): IApiEmail {
    return {
      email_id: email.emailId,
      email_address: email.emailAddress,
      person_id: email.personId
    };
  }
}