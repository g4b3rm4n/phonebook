import { IApiPerson } from '../interfaces/api-person.interface';
import { Email } from './email';
import { Address } from './address';
import { PhoneNumber } from './phone-number';
import { IApiAddress } from '../interfaces/api-address.interface';
import { IApiEmail } from '../interfaces/api-email.interface';
import { IApiPhoneNumber } from '../interfaces/api-phone-number.interface';

export class Person {
  public personId: number;
  public firstName: string;
  public lastName: string;
  public emails: Email[];
  public addresses: Address[];
  public phoneNumbers: PhoneNumber[];

  constructor(person: IApiPerson) {
    this.personId = person.person_id;
    this.firstName = person.first_name;
    this.lastName = person.last_name;

    if (person.emails) {
      this.emails = person.emails.map((e: IApiEmail): Email => new Email(e));
    }

    if (person.addresses) {
      this.addresses = person.addresses.map((a: IApiAddress): Address => new Address(a));
    }

    if (person.phone_numbers) {
      this.phoneNumbers = person.phone_numbers.map((p: IApiPhoneNumber): PhoneNumber => new PhoneNumber(p));
    }
  }

  public static convertToApiObject(person: Person): IApiPerson {
    return {
      person_id: person.personId,
      first_name: person.firstName,
      last_name: person.lastName
    };
  }
}