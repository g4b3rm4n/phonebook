import { IApiPhoneNumber } from '../interfaces/api-phone-number.interface';

export class PhoneNumber {
  public phoneNumberId: number;
  public phoneNumber: string;
  public personId: number;

  constructor(phoneNumber: IApiPhoneNumber) {
    this.phoneNumberId = phoneNumber.phone_number_id;
    this.phoneNumber = phoneNumber.phone_number;
    this.personId = phoneNumber.person_id;
  }

  public static convertToApiObject(phoneNumber: PhoneNumber): IApiPhoneNumber {
    return {
      phone_number_id: phoneNumber.phoneNumberId,
      phone_number: phoneNumber.phoneNumber,
      person_id: phoneNumber.personId
    };
  }
}