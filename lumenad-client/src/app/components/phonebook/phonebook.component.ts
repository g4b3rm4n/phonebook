import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import { PersonService } from 'src/app/services/person.service';
import { Person } from 'src/app/classes/person';
import { PersonComponent } from '../person/person.component';

@Component({
  selector: 'app-phonebook',
  templateUrl: './phonebook.component.html',
  styleUrls: ['./phonebook.component.scss']
})
export class PhonebookComponent implements OnInit, AfterViewInit {

  public selectedPerson: Person;
  public people: Person[];

  @ViewChild(PersonComponent) private personComponent: PersonComponent;

  constructor(
    private personService: PersonService
  ) {
    this.people = [];
  }

  private getPeople(people: Person[]): void {
    this.people = people;
  }

  private removePerson(person: Person): void {
    if (person === this.selectedPerson) {
      this.selectedPerson = null;
    }

    const index = this.people.indexOf(person);
    this.people.splice(index, 1);
  }

  private onPersonAdded(person: Person): void {
    this.people.push(person);
  }

  private onPersonChanged(person: Person): void {
    const personIds = this.people.map((p: Person) => p.personId);
    const personIndex = personIds.indexOf(person.personId);
    this.people.splice(personIndex, 1, person);
  }

  public setSelectedPerson(event: Event, person: Person): void {
    event.preventDefault();

    this.selectedPerson = person;
  }

  public onPersonDeleteClicked(person: Person): void {
    this.personService.deletePerson(person).subscribe(this.removePerson.bind(this, person));
  }

  public onPersonAddClicked(): void {
    this.selectedPerson = new Person({
      person_id: undefined,
      first_name: '',
      last_name: '',
    });
  }

  public ngOnInit(): void {
    this.personService.getPeople().subscribe(this.getPeople.bind(this));
  }

  public ngAfterViewInit(): void {
    this.personComponent.personAdded.subscribe(this.onPersonAdded.bind(this));
    this.personComponent.personChanged.subscribe(this.onPersonChanged.bind(this));
  }
}
