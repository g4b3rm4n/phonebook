import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Person } from 'src/app/classes/person';
import { Email } from 'src/app/classes/email';
import { Address } from 'src/app/classes/address';
import { PhoneNumber } from 'src/app/classes/phone-number';
import { PersonService } from 'src/app/services/person.service';
import { EmailService } from 'src/app/services/email.service';
import { AddressService } from 'src/app/services/address.service';
import { PhoneNumberService } from 'src/app/services/phone-number.service';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit, OnChanges {

  @Output() public personChanged: EventEmitter<Person>;
  @Output() public personAdded: EventEmitter<Person>;

  @Input() public person: Person;
  public emails: Email[];
  public addresses: Address[];
  public phoneNumbers: PhoneNumber[];

  public form: FormGroup;

  constructor(
    private personService: PersonService,
    private emailService: EmailService,
    private addressService: AddressService,
    private phoneNumberService: PhoneNumberService
  ) {
    this.personChanged = new EventEmitter<Person>();
    this.personAdded = new EventEmitter<Person>();
  }

  private createFormGroupFromEmail(email: Email): FormGroup {
    return new FormGroup({
      'emailId': new FormControl(email.emailId),
      'emailAddress': new FormControl(email.emailAddress, Validators.email),
      'personId': new FormControl(email.personId),
    });
  }

  private createFormGroupFromAddress(address: Address): FormGroup {
    return new FormGroup({
      'addressId': new FormControl(address.addressId),
      'country': new FormControl(address.country),
      'city': new FormControl(address.city),
      'region': new FormControl(address.region),
      'zipCode': new FormControl(address.zipCode),
      'street': new FormControl(address.street),
      'houseNumber': new FormControl(address.houseNumber, Validators.min(0)),
      'personId': new FormControl(address.personId),
    });
  }

  private createFormGroupFromPhoneNumber(phoneNumber: PhoneNumber): FormGroup {
    return new FormGroup({
      'phoneNumberId': new FormControl(phoneNumber.phoneNumberId),
      'phoneNumber': new FormControl(phoneNumber.phoneNumber),
      'personId': new FormControl(phoneNumber.personId),
    });
  }

  private createForms() {
    this.form = new FormGroup({
      'personForm': new FormGroup({
        'personId': new FormControl(this.person.personId),
        'firstName': new FormControl(this.person.firstName, Validators.required),
        'lastName': new FormControl(this.person.lastName, Validators.required),
      }),
      'emailsForms': new FormArray(this.person.emails.map(this.createFormGroupFromEmail)),
      'addressesForms': new FormArray(this.person.addresses.map(this.createFormGroupFromAddress)),
      'phoneNumbersForms': new FormArray(this.person.phoneNumbers.map(this.createFormGroupFromPhoneNumber))
    });
  }

  private setPerson(p: Person): void {
    this.person = p;
    this.createForms();
  }

  private gatherPersonData(): void {
    this.personService.getPerson(this.person.personId).subscribe(this.setPerson.bind(this));
  }

  public finalizeEmailChanges(): void {
    const changeEmail = (email: Email): void => {
      let emailStored = this.person.emails.find((e: Email) => e.emailId === email.emailId);
      emailStored = email;
    }

    const addEmail = (email: Email): void => {
      this.person.emails.push(email);
    }

    const handleEmailField = (emailField: FormGroup) => {
      if (typeof emailField.get('emailId').value === 'number') {
        this.emailService.modifyEmail(emailField.getRawValue() as Email).subscribe(changeEmail);
      } else {
        this.emailService.addEmail(emailField.getRawValue() as Email).subscribe(addEmail);
      }
    };

    const removeDeletedEmail = (email: Email): void => {
      const emailIds = this.emailsForms.getRawValue().map((e: Email) => e.emailId);

      const emailStillExists = (email: Email): boolean => {
        return emailIds.indexOf(email.emailId) > -1;
      }

      const removeEmail = (email: Email): void => {
        const index = this.person.emails.findIndex((e: Email): boolean => {
          return e.emailId === email.emailId;
        });

        this.person.emails.splice(index, 1);
      }

      if (!emailStillExists(email)) {
        this.emailService.deleteEmail(email).subscribe(removeEmail.bind(this, email));
      }
    }

    this.emailsForms.controls.forEach(handleEmailField);
    this.person.emails.forEach(removeDeletedEmail.bind(this));
  }

  public finalizeAddressChanges(): void {
    const changeAddress = (address: Address): void => {
      let addressStored = this.person.addresses.find((a: Address) => a.addressId === address.addressId);
      addressStored = address;
    }

    const addAddress = (address: Address): void => {
      this.person.addresses.push(address);
    }

    const handleAddressField = (addressField: FormGroup) => {
      if (typeof addressField.get('addressId').value === 'number') {
        this.addressService.modifyAddress(addressField.getRawValue() as Address).subscribe(changeAddress);
      } else {
        this.addressService.addAddress(addressField.getRawValue() as Address).subscribe(addAddress);
      }
    };

    const removeDeletedAddress = (address: Address): void => {
      const addressIds = this.addressesForms.getRawValue().map((a: Address) => a.addressId);

      const addressStillExists = (address: Address): boolean => {
        return addressIds.indexOf(address.addressId) > -1;
      }

      const removeAddress = (address: Address): void => {
        const index = this.person.addresses.findIndex((a: Address): boolean => {
          return a.addressId === address.addressId;
        });

        this.person.addresses.splice(index, 1);
      }

      if (!addressStillExists(address)) {
        this.addressService.deleteAddress(address).subscribe(removeAddress.bind(this, address));
      }
    }

    this.addressesForms.controls.forEach(handleAddressField);
    this.person.addresses.forEach(removeDeletedAddress.bind(this));
  }

  public finalizePhoneNumberChanges(): void {
    const changePhoneNumber = (phoneNumber: PhoneNumber): void => {
      let phoneNumberStored = this.person.phoneNumbers.find((p: PhoneNumber) => p.phoneNumberId === phoneNumber.phoneNumberId);
      phoneNumberStored = phoneNumber;
    }

    const addPhoneNumber = (phoneNumber: PhoneNumber): void => {
      this.person.phoneNumbers.push(phoneNumber);
    }

    const handlePhoneNumberField = (phoneNumberField: FormGroup) => {
      if (typeof phoneNumberField.get('phoneNumberId').value === 'number') {
        this.phoneNumberService.modifyPhoneNumber(phoneNumberField.getRawValue() as PhoneNumber).subscribe(changePhoneNumber);
      } else {
        this.phoneNumberService.addPhoneNumber(phoneNumberField.getRawValue() as PhoneNumber).subscribe(addPhoneNumber);
      }
    };

    const removeDeletedPhoneNumber = (phoneNumber: PhoneNumber): void => {
      const phoneNumberIds = this.phoneNumbersForms.getRawValue().map((p: PhoneNumber) => p.phoneNumberId);

      const phoneNumberStillExists = (phoneNumber: PhoneNumber): boolean => {
        return phoneNumberIds.indexOf(phoneNumber.phoneNumberId) > -1;
      }

      const removePhoneNumber = (phoneNumber: PhoneNumber): void => {
        const index = this.person.phoneNumbers.findIndex((p: PhoneNumber): boolean => {
          return p.phoneNumberId === phoneNumber.phoneNumberId;
        });

        this.person.phoneNumbers.splice(index, 1);
      }

      if (!phoneNumberStillExists(phoneNumber)) {
        this.phoneNumberService.deletePhoneNumber(phoneNumber).subscribe(removePhoneNumber.bind(this, phoneNumber));
      }
    }

    this.phoneNumbersForms.controls.forEach(handlePhoneNumberField);
    this.person.phoneNumbers.forEach(removeDeletedPhoneNumber.bind(this));
  }


  private handleSave() {
    this.finalizeEmailChanges();
    this.finalizeAddressChanges();
    this.finalizePhoneNumberChanges();

    this.form.markAsPristine();
  }

  private updatePerson(): void {
    this.personService.modifyPerson(this.personForm.getRawValue()).subscribe(
      (person: Person) => {
        this.person.personId = person.personId;
        this.person.firstName = person.firstName;
        this.person.lastName = person.lastName;
        
        this.handleSave();

        this.personChanged.emit(this.person);
      }
    );
  }

  private addPerson(): void {
    this.personService.addPerson(this.personForm.getRawValue()).subscribe(
      (person: Person) => {
        this.person.personId = person.personId;
        this.person.firstName = person.firstName;
        this.person.lastName = person.lastName;

        this.handleSave();

        this.personAdded.emit(this.person);
      }
    );
  }

  public onAddEmail(): void {
    this.emailsForms.push(this.createFormGroupFromEmail(
      new Email({
        email_id: null,
        email_address: null,
        person_id: this.person.personId
      })
    ));
  }

  public onAddAddress(): void {
    this.addressesForms.push(this.createFormGroupFromAddress(
      new Address({
        address_id: null,
        city: null,
        country: null,
        house_number: null,
        region: null,
        street: null,
        zip_code: null,
        person_id: this.person.personId
      })
    ));
  }

  public onAddPhoneNumber(): void {
    this.phoneNumbersForms.push(this.createFormGroupFromPhoneNumber(
      new PhoneNumber({
        phone_number_id: null,
        phone_number: null,
        person_id: this.person.personId
      })
    ));
  }

  public onEmailDeleted(index: number): void {
    this.emailsForms.removeAt(index);
    this.emailsForms.markAsDirty();
    this.emailsForms.markAsTouched();
  }

  public onAddressDeleted(index: number): void {
    this.addressesForms.removeAt(index);    
    this.addressesForms.markAsDirty();
    this.addressesForms.markAsTouched();
  }

  public onPhoneNumberDeleted(index: number): void {
    this.phoneNumbersForms.removeAt(index);
    this.phoneNumbersForms.markAsDirty();
    this.phoneNumbersForms.markAsTouched();
  }

  public onSave(): void {
    if (typeof this.person.personId === 'number') {
      this.updatePerson();
      return;
    }

    this.addPerson();
  }

  public ngOnInit(): void {
  }

  public ngOnChanges(): void {
    if (!this.person) {
      return;
    }

    if (typeof this.person.personId === 'number') {
      this.gatherPersonData();
      return;
    }

    this.person.emails = [];
    this.person.addresses = [];
    this.person.phoneNumbers = [];

    this.createForms();
  }

  public get personForm(): FormGroup {
    return this.form.get('personForm') as FormGroup;
  }

  public get emailsForms(): FormArray {
    return this.form.get('emailsForms') as FormArray;
  }

  public get addressesForms(): FormArray {
    return this.form.get('addressesForms') as FormArray;
  }

  public get phoneNumbersForms(): FormArray {
    return this.form.get('phoneNumbersForms') as FormArray;
  }
}
