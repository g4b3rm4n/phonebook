import { IApiEmail } from './api-email.interface';
import { IApiAddress } from './api-address.interface';
import { IApiPhoneNumber } from './api-phone-number.interface';

export interface IApiPerson {
  person_id: number;
  first_name: string;
  last_name: string;
  emails?: IApiEmail[];
  addresses?: IApiAddress[];
  phone_numbers?: IApiPhoneNumber[];
}