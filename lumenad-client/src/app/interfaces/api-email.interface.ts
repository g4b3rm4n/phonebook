export interface IApiEmail {
  email_id: number;
  email_address: string;
  person_id: number;
}