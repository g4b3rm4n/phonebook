export interface IApiAddress {
  address_id: number;
  country: string;
  city: string;
  region: string;
  zip_code: string;
  street: string;
  house_number: number;
  person_id: number;
}