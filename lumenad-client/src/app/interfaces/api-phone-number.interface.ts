export interface IApiPhoneNumber {
  phone_number_id: number;
  phone_number: string;
  person_id: number;
}