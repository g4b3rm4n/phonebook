# Instructions

## Project Overview

Create an contact (address book) web application in a chosen stack. This will include the database, the
business logic (example laravel) and the front-end (react or view). The scope of this project is up to
you. You can just deliver only diagrams of the database and framework components. Or you can deliver a
functional coded application. The amount of work you put into it is entirely up to you.

## Project Goal

What ever you deleiver to us, should "prove" to us that you are a senior PHP developer. The purpose is
only to help us get a good grasp of your skillset. 

## Project Requirements

- The ability to add new contacts
- The ability to edit contacts
- The ability to delete contacts
- The ability to View all Contacts by name in a list
- The ability to view a single contact with information
- Each contact should have the following Fields:
  - Required:
    - First name
    - Last name
  - Optional:
    - emails
    - phone
    - address
    - city
    - region
    - zip code
- Ability to run this application locally on YOUR computer. You can screen share to demo it for us. 
- Please push your code to a Git repository (Github or Bitbucket) and share the repository link with us.
- The ability for multiple emails, phone numbers, and addresses for a single contact
- Database diagrams of what this database would look like
- A good UI experience (this isn't as important as functionality)...Functionality first

## Project Iterations

- What other features would you add?
- What features are lacking?
- How would you itterate on this application to improve it?

# Starting the project

The project will include a backend made in Laravel (tutorial used is [here](https://www.toptal.com/laravel/restful-laravel-api-tutorial)) and frontend made with Angular 2+.

## Database

Looking at the specification we'll need the following:

- person
  - person_id : number, pk, ai
  - first_name : varchar(128)
  - last_name : varchar(128)

- email
  - email_id : number, pk, ai
  - email_address : varchar(128)
  - person_id : number, fk

- phone_number
  - phone_number_id : number, pk, ai
  - phone_number : varchar(32) _NOTE:_ we'll use string because of the `+` for international numbers
  - person_id : number, fk

- address
  - country : varchar(128)
  - city : varchar(128)
  - region : varchar(128)
  - zip_code : varchar(16) _NOTE:_ for example in the Czech Republic it can contain spaces
  - street : varchar(128)
  - house_number : number
  - person_id : number, fk

### Seeding the database

For seeding the database we'll use 10 random users and 20-20 emails, phone numbers and addresses that will
each have the connection with a random user.

# Possible improvements

## Having pictures for the entries

People could have a caller picture (or even more pictures); the data could even be stored in MySQL! (though that'd
make caching impossible, so maybe not).

## Extending the `Person` class to have more attributes

... such as birthday, workplace and of course a small description so you know who's calling from Vietnam. (["Since approximately 40 percent of all Vietnamese people have the surname Nguyễn"](https://en.wikipedia.org/wiki/Nguyen))

## Adding/Modifying a person should be possible with only one API call

Instead of adding a person and then adding any number of emails, phone numbers and addresses taking each one API call
we could have an extra API endpoint for `people` that can take the whole JSON and would create the appropriate entries
in the database. Since it's weekend and it's hot and I just had a coffee but it doesn't make me have energy anymore I'll just proceed with doing the frontend soon. That's gonna take some time.

## Authentication

We could implement proper authentication for the application.

## Optimize for mobile

It shouldn't be hard to have the proper CSS for different display sizes.

## Redesign

Just look at it. #time

## Tests

_Obviously._

## Redesign application to have edit mode

Right now when selecting a person it'll have all it's data writable. Since saving is not atomic on the frontend side (the user needs to click 'Save' in order for the
changes to take effect) it's not a huge issue, but all the data in little boxes looks a bit ehm. Also, we could move the placeholders to be labels, though that would
look weird for emails and phone numbers.

# Other notes

- I have extended the address to have a lot more things - I think otherwise that would've counted as things to improve